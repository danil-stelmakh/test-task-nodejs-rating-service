local group = redis.call('HGET', 'user-groups', KEYS[1]); 

if not group then 
    group = math.floor(redis.call('HLEN', 'user-groups') / 20); 
    redis.call('HSET','user-groups', KEYS[1], group); 
end;

redis.call('ZINCRBY', 'group-' .. group, KEYS[2], KEYS[1]); 
local groupTop = redis.call('ZREVRANGEBYSCORE', 'group-' .. group, '+inf', '-inf', 'WITHSCORES', 'LIMIT', 0, 1); 
redis.call('HSET','winners', group, groupTop[1] .. ':' .. groupTop[2]);  

return group;