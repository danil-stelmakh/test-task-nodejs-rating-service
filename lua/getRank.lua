local group = redis.call('HGET', 'user-groups', KEYS[1]); 

if not group then 
    return 0;
end;

return {redis.call('ZREVRANK', 'group-' .. group, KEYS[1]), tonumber(redis.call('ZSCORE', 'group-' .. group, KEYS[1]))};