Тестовое задание: микросервис хранения рейтингов пользователей.
Требования:
- Сервис должен быть написан на node.js
- ...

Для хранения рейтингов решил использовать **redis**. Сортированые множества для этого подходят очень хорошо. 

Разделение на группы по 20 пользователей происходит с помощью lua скрипта выполняемого в redis  **./lua/incRating.lua** 
- Он принимает на вход `id` пользователя и `incr` на сколько необходимо изменить рейтинг;
- Ищет группу пользователя в хеше `user-groups`;
- Если пользователь ещё не размещался в группе, то происходит добавление его в хеш `user-groups`, номер группы для размещения вычисляется по кол-ву запесей в хеше `math.floor(HLEN user-groups / 20)`;
- Инкрементит значение рейтинга в для ID в группе пользователя `ZINCRBY group-N id incr`
- Тут же получает текущего победителя в этой-же группе и запоминает его в хеше `winners` в виде строки `id:incr`. 
  
Решил что будет удобнее постойнно поддерживать актуальный списоок победителей в хеше `winners` чем делать 100500 запросов к каждой группе. Но если запрос списка победителей планируется вызывать не часто, то можно отказатся от этой избыточности.

Так же написан lua скрипт **./lua/getRating** принимает на вход `id` пользователя, смотрит в какой группе он размещен, и если находит, то возваращает кол-во очков и текущую позицию в рейтинге, если же нет, то возвращает 0;

Сброс рейтингов осуществляется вызовом команды `FLUSHDB`, поэтому для работы сервиса необходимо выделит отдельную базу редиса.
