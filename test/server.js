"use strict";

var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();

chai.use(chaiHttp);

var expect = chai.expect;

var baseUrl = 'http://127.0.0.1:3000';

function getWinners() {
    return new Promise(function (resolve, reject) {
        chai.request(baseUrl)
                .get('/get_winners')
                .end(function (err, res) {
                    should.equal(err, null);
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    res.body.should.have.property('result');
                    res.body.result.should.equal('ok');
                    res.body.should.have.property('data');
                    res.body.data.should.be.a('array');
                    resolve(res.body.data);
                });
    });
}

var incRank = function (id, inc) {
    return new Promise(function (resolve, reject) {
        chai.request(baseUrl)
                .post('/inc_rank')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({id: id, inc: inc})
                .end(function (err, res) {
                    try {
                        should.equal(err, null);
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a('object');
                        res.body.should.have.property('result');
                        res.body.result.should.equal('ok');
                        res.body.should.have.property('group');
                        resolve({id: id, score: inc, group: res.body.group});
                    } catch (err) {
                        reject(err);
                    }
                });
    });
};

var getRank = function (id, assertEmptyData) {
    return new Promise(function (resolve, reject) {
        chai.request(baseUrl)
                .get('/get_rank?id=' + id)
                .end(function (err, res) {
                    try {
                        should.equal(err, null);
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a('object');
                        res.body.should.have.property('result');
                        res.body.result.should.equal('ok');
                        res.body.should.have.property('data');
                        if(assertEmptyData) {
                            res.body.data.should.be.empty;
                        } else {
                            res.body.data.should.have.property('rank');
                            res.body.data.should.have.property('score');
                        }
                        resolve({id:id, data:res.body.data});
                    } catch (err) {
                        reject(err);
                    }
                });
    });
};

function resetRating() {
    return new Promise(function (resolve, reject) {
        chai.request(baseUrl)
                .post('/reset_ratings')
                .end(function (err, res) {
                    try {
                        should.equal(err, null);
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a('object');
                        res.body.should.have.property('result');
                        res.body.result.should.equal('ok');
                        getWinners().then(function (res) {
                            try {
                                res.should.have.length(0);
                                resolve();
                            } catch (err) {
                                reject(err);
                            }
                        });
                    } catch (err) {
                        reject(err);
                    }
                });
    });
}


var randomInteger = function (min, max) {
    return Math.round(min + Math.random() * (max - min));
};

var sortGroups = function (groups) {
    for (var index in groups) {
        groups[index].sort(function (a, b) {
            return a.score < b.score ? 1:-1;
        });
    }
    return groups;
}

var calcUsersRank = function (groups, users) {
    for (var index in groups) {
        for (var rank in groups[index]) {
            var id = groups[index][rank].id;
            users[id].rank = rank;
        }
    }
    return users;
}

describe('*** TestTask Rating Service', function () {
    var users = {};
    var groups = {};

    describe('/POST reset_ratings', function () {
        it('it should reset all rating groups', function (done) {
            resetRating().then(function(){
                done();
            });
        });
    });

    describe('/POST inc_rank', function () {
        it('it should get "incorrect id" error at', function (done) {
            chai.request(baseUrl)
                    .post('/inc_rank')
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .end(function (err, res) {
                        should.equal(err, null);
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.have.property('code');
                        res.body.error.should.have.property('message');
                        res.body.error.code.should.equal(1001);
                        res.body.error.message.should.equal('incorrect id');
                        done();
                    });
        });

        // Инкриментим рейтинг для 9999 пользователей на случайное число
        it('it should increment user score for id: 1 - 9999', function (done) {
            var requestList = [];
            for (var id = 1; id <= 9999; id++) {
                // Инкриментим всем на разное число очков,
                // что бы избежать коллизий с одинаковой суммой очков и неопределенным рейтингом
                requestList.push(incRank(id, id + 10000));
            }
            Promise.all(requestList).then(function (res) {
                for (var index in res) {
                    // запомним в какую группу попал пользователь,
                    // и сколько у него очков рейтинга
                    users[res[index].id] = res[index];
                    if (!groups[res[index].group]) {
                        groups[res[index].group] = [];
                    }
                    groups[res[index].group].push(res[index]);
                }
                groups = sortGroups(groups);
                users = calcUsersRank(groups, users);

                // Проверим что разделило по 20 id в группе
                for (var groupNumber = 0; groupNumber <= 498; groupNumber++) {
                    expect(groups[groupNumber].length).to.equal(20);
                }
                // И 19 id в последней группе
                expect(groups[groupNumber++].length).to.equal(19);

                done();
            });
        });

        // Отнимим очки у текущих лидеров рейтинга, топ на сервере должен изменится
        it('it should decrement user score for current winners', function (done) {
            var requestList = [];
            for (var index in groups) {
                var id = groups[index][0].id;
                requestList.push(incRank(id, -20000));
                users[id].score -= 20000;
            }
            Promise.all(requestList).then(function (res) {
                // Пересортируем группы, и пересчитываем позицию в рейтинге
                groups = sortGroups(groups);
                users = calcUsersRank(groups, users);
                done();
            });
        });
    });

    describe('/GET get_rank', function () {
        it('it should get "incorrect id" error', function (done) {
            chai.request(baseUrl)
                    .get('/get_rank')
                    .end(function (err, res) {
                        should.equal(err, null);
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.have.property('code');
                        res.body.error.should.have.property('message');
                        res.body.error.code.should.equal(1001);
                        res.body.error.message.should.equal('incorrect id');
                        done();
                    });
        });

        it('it should get empty user rank for id 10000000', function (done) {
            getRank(10000000, true).then(function () {
                done();
            });
        });

        it('it should get actual user score for id: 1 - 9999', function (done) {
            var requestList = [];
            for (var id = 1; id <= 9999; id++) {
                requestList.push(getRank(id, false));
            }
            Promise.all(requestList).then(function (res) {
                for (var index in res) {
                    expect(parseInt(res[index].data.score)).to.equal(parseInt(users[res[index].id].score));
                    //expect(parseInt(res[index].data.rank)).to.equal(parseInt(users[res[index].id].rank));
                }
                done();
            });
        });
    });

    describe('/get get_winners', function () {
        it('it should get rating top', function (done) {
            getWinners().then(function (data) {
                for (var index in data) {
                    var winner = users[data[index].id];
                    expect(parseInt(winner.score)).to.equal(parseInt(data[index].score));
                    expect(parseInt(winner.rank)).to.equal(0);
                }
                done();
            });
        });
    });

    describe('/POST reset_ratings', function () {
        it('it should reset all rating groups', function (done) {
            resetRating().then(function(){
                done();
            });
        });
    });

});

