var express = require('express');
var bodyParser = require('body-parser');
var redis = require("ioredis");
var fs = require('fs');

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));

var redisClient = new redis({
    port: 6379,
    host: '127.0.0.1',
    db: 5
});

// Скрипт для размещения пользователя в группе (если не был размещён ранее) и инкримента рейтинга
redisClient.defineCommand('incRank', {
    numberOfKeys: 2,
    lua: fs.readFileSync('./lua/incRank.lua')
});

// Скрипт для получения рейтинга пользователя
redisClient.defineCommand('getRank', {
    numberOfKeys: 1,
    lua: fs.readFileSync('./lua/getRank.lua')
});

/**
 * инкрементит рейтинг пользователя на переданное количество очков,
 * принимает два параметра: id пользователя и количество очков (Значение может быть отрицательным)
 */
app.post('/inc_rank', function (req, res) {
    var id = req.body.id ? parseInt(req.body.id) : 0;
    var inc = req.body.inc ? parseInt(req.body.inc) : 0;
    if (id <= 0) {
        return res.json({error: {code: 1001, message: "incorrect id"}});
    }
    redisClient.incRank(id, inc, function (err, result) {
        if (err instanceof redis.ReplyError) {
            res.json({error: {code: 1002, message: "storage error"}});
        } else {
            res.json({result: "ok", group: result});
        }
    });
});

/**
 * возвращает текущее количество очков пользователя и его место в рейтинге его группы,
 * принимает id пользователя в качестве параметра
 * response.data.rank позиция в рейтинге начиная с нуля
 * response.data.score количество очков пользователя
 * если для пользователя не вызывался inc_rank, то response.data - пустой объект
 */
app.get('/get_rank', function (req, res) {
    var id = req.query.id ? parseInt(req.query.id) : 0;
    if (id <= 0) {
        return res.json({error: {code: 1001, message: "incorrect id"}});
    }
    redisClient.getRank(id, function (err, result) {
        if (err instanceof redis.ReplyError) {
            res.json({error: {code: 1002, message: "storage error"}});
        } else if (result) {
            res.json({result: "ok", data: {rank: result[0], score: result[1]}});
        } else {
            // Не разу не вызывали inc_rank для этого ID
            res.json({result: "ok", data: {}});
        }
    });
});

/**
 * возвращет всех победителей, т.е. пользователей на первом месте в своих группах.
 * Возвращает массив объектов с id пользователей и очками.
 */
app.get('/get_winners', function (req, res) {
    redisClient.hgetall('winners', function (err, result) {
        if (err instanceof redis.ReplyError) {
            res.json({error: {code: 1002, message: "storage error"}});
        } else {
            var resultArray = [];
            for (var key in result) {
                var parts = result[key].split(':');
                resultArray.push({id: parts[0], score:parts[1]});
            }
            res.json({result: "ok", data: resultArray});
        }
    });
});

/**
 * сбрасывает все группы и рейтинги, после этого все рейтинги считаются заново.
 */
app.post('/reset_ratings', function (req, res) {
    redisClient.flushdb(function (err, result) {
        if (err instanceof redis.ReplyError) {
            res.json({error: {code: 1002, message: "storage error"}});
        } else {
            res.json({result: "ok"});
        }
    });
});

app.listen(3000);
